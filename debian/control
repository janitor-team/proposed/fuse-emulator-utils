Source: fuse-emulator-utils
Priority: optional
Maintainer: Alberto Garcia <berto@igalia.com>
Build-Depends: debhelper (>= 10),
               zlib1g-dev,
               libjpeg-dev,
               libpng-dev,
               libaudiofile-dev,
               libgcrypt20-dev,
               libglib2.0-dev,
               libspectrum-dev (>= 1.4.3)
Standards-Version: 4.1.5
Section: otherosfs
Homepage: http://fuse-emulator.sourceforge.net/
Vcs-Browser: https://salsa.debian.org/berto/fuse-emulator-utils
Vcs-Git: https://salsa.debian.org/berto/fuse-emulator-utils.git

Package: fuse-emulator-utils
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: The Free Unix Spectrum Emulator - Utilities
 The Fuse utilities are a few tools which may be of occasional use when
 dealing with ZX Spectrum emulator files. They were originally
 distributed with Fuse, the Free Unix Spectrum Emulator, but are now
 independent of Fuse and can be used on their own.
 .
 The available utilities are:
   * audio2tape: convert an audio file to tape format.
   * createhdf: create an empty .hdf IDE hard disk image.
   * fmfconv: converter tool for FMF movie files.
   * listbasic: list the BASIC in a snapshot or tape file.
   * profile2map: convert Fuse profiler output to Z80-style
     map format.
   * raw2hdf: create a .hdf IDE hard disk image from another file.
   * rzxcheck: verify the digital signature in an RZX file.
   * rzxdump: list the contents of an RZX input recording file.
   * rzxtool: add, extract or remove the embedded snapshot from
     an RZX file, or compress or uncompress the file.
   * scl2trd: convert .scl disk images to .trd disk images.
   * snap2tzx: convert snapshots to TZX tape images.
   * snapconv: convert between snapshot formats.
   * snapdump: list contents of snapshot files.
   * tape2pulses: dumps the pulse information from tape images to text
     files.
   * tape2wav: convert a tape file to .wav audio format.
   * tapeconv: convert between .tzx and .tap files.
   * tzxlist: list the contents of a TZX, TAP, PZX or Warajevo TAP
     file.
